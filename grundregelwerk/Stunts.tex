\chapter{Stunts}\label{cha:stunts} 

Wenn du in bestimmten Situationen glänzen möchtest, eine besondere Fähigkeit anwenden willst oder gern außergewöhnliche Kräfte hättest, dann brauchst du einen Stunt.
Einerseits können dir Stunts helfen, deine Fertigkeiten zu erweitern: Du kannst eine Fertigkeit in Situationen einsetzen, für die du eigentlich eine andere Fertigkeit brauchen würdest, oder deine Fertigkeit kann eine andere Fertigkeit ergänzen. Es ist sogar möglich, unter bestimmten Umständen zwei zusätzliche Erfolgsstufen zu bekommen. Auch andere kleinere Effekte sind drin. Anders gesagt, Stunts ermöglichen es dir, die Regeln für die Fertigkeiten zu brechen oder zumindest zu biegen.
Ihr dürft gern eigene Stunts erschaffen und so das Spiel an eure Vorstellungen anpassen – wir möchten euch sogar ausdrücklich dazu raten. Um diesen Prozess etwas zu erleichtern, gibt es fünf Schablonen für Stunts. Die Schablonen bieten euch Richtlinien zum Erstellen und Ausbalancieren der neuen Stunts. Am besten schreibt ihr die Schablonennamen bei jedem erschaffenen Stunt dazu, damit ihr später den Bezug leichter finden könnt. Damit könnt ihr auch umfangreiche Stuntlisten aufbauen.

\section{Stuntschablonen}
\subsection{Fokus}\index{Stunt!Fokus}
Ein Fokusstunt gibt deinem Charakter einen +2 Bonus auf eine Fertigkeit, wenn die Bedingungen stimmen. Der Anwendungsbereich hierfür kann sehr breit angelegt sein. Dennoch kommt ein Fokus nicht bei jeder Fertigkeitsanwendung zur Geltung.
\begin{commentbox}{Erklärung}
	Zum Beispiel wäre Klingenwaffen ein Fokus für eine Kampffertigkeit. Er kommt nur zum Einsatz, wenn Waffen mit einer Klinge, wie Schwerter, Messer oder Äxte, benutzt werden. Dein Charakter würde dann einen +2 Bonus auf Kampffertigkeits-Proben beim Einsatz solcher Waffen bekommen, aber nicht, wenn er Knüppel, Stäbe oder Peitschen verwendet.
\end{commentbox}
\begin{commentbox}{Beispiel}
Peter erstellt mit Der SL den Fokus Polizeispitzel für seinen Charakter. Djego bekommt einen +2 Bonus auf die Fertigkeit Einschätzen.
\end{commentbox}

\subsection{Spezialisierung}\index{Stunt!Spezialisierung}
Eine Spezialisierung bringt deinem Charakter beim Anwenden einer Fertigkeit einen Bonus von +4, wenn die Voraussetzungen des Stunts erfüllt sind.
Für die Fertigkeit Nahkampf ist die Spezialisierung auf eine bestimmte Waffe – vielleicht ein Katana? – ein guter Stunt. Der Charakter bekommt den Bonus von +4 aber auch nur, wenn er mit einem Katana kämpft.
Wenn du zusätzlich einen Fokusstunt hast, der sich mit der Spezialisierung überlagert, dann gilt nur der +4 Bonus der Spezialisierung.
\begin{commentbox}{Beispiel}
Peter überlegt sich auch eine Spezialisierung und nennt diese Polizeiverhör. Er erhält einen +4 Bonus auf Einschüchtern, wenn er ein Verhör in einer Polizeistation durchführt.
\end{commentbox}

\subsection{Fertigkeiten-tausch}\index{Stunt!Tausch}
Diese Stuntschablone erlaubt es dir, eine Fertigkeit anstelle einer anderen zu verwenden, wenn gewisse Bedingungen erfüllt sind. Der Anwendungsbereich von Fertigkeiten-tausch ist ähnlich wie beim Fokus.
Ein Fertigkeiten-tausch kann es dir z. B. erlauben, Akrobatik anstelle von Reiten zu verwenden, wenn du ein Tier reitest.
\begin{commentbox}{Beispiel}
Gemeinsam entwickeln Peter und Der SL den Stunt Fertigkeiten-tausch Umgebung Lesen. Damit kann Peter Suchen anstelle von Einschätzen benutzten, um die Gefühle von Personen zu lesen. Dazu muss die betroffene Person in ihrem eigenen Haus, an ihrem Arbeitsplatz, in ihrem Auto oder an ihrem bevorzugten Aufenthaltsort befragt werden. Wird die gleiche Person an einem anderem Ort befragt, muss Peter seinen Einschätzen-Wert verwenden.
\end{commentbox}

\subsection{Verbündeter}\index{Stunt!Verbündeter}
Durch diesen Stunt erhältst du einen Verbündeten, also jemanden, der dir innerhalb und außerhalb von Konflikten hilfreich zur Seite stehen kann. Ein Verbündeter ist in der Regel ein~\prettyref{sec:gefährte} bzw~\prettyref{sec:tiergefährte}.

\subsection{Vorteil}\index{Stunt!Vorteil}
Ein Vorteil-Stunt ist eine allgemeine Vorlage für all jene Stunts, die nicht mit einer der vorangegangenen vier Schablonen abgedeckt werden. Mit einem Vorteil kann dein SC Zugang zu Ressourcen, Gegenständen, anderen speziellen Fähigkeiten und Eigenschaften erhalten.
Manche Vorteil-Stunts mögen euch ziemlich stark erscheinen – aber wenn ihr euch alle einig seid, dann könnt ihr sie natürlich gern verwenden. Wenn ihr aber der Meinung seid, das sie Ungleichgewicht ins Spiel bringen, lasst sie weg oder verändert sie so, dass sie in eure Spielwelt passen. Einige Möglichkeiten, wie ihr Stunts anpassen könnt, findet ihr im Folgenden beschrieben.

\section{Stunts anpassen}\index{Stunt!Anpassen}
\subsection{Schicksal-Punkte einsetzen}
Am einfachsten begrenzt ihr einen zu mächtigen Stunt, indem ihr den Stunteinsatz an die Ausgabe eines Schicksal-Punktes koppelt.
\begin{commentbox}{Beispiel}
Marco will einen Stunt, der seinem Charakter Mickey erlaubt, in jedem Konflikt als Erster handeln zu können. Der SL findet das zu mächtig, erlaubt es aber, wenn Marco bereit ist, jedes Mal einen Schicksal-Punkt dafür zu zahlen. Diesem, nun ausgewogeneren, Stunt gibt Marco den Namen Ich bin immer der Erste!.
\end{commentbox}

\subsection{Voraussetzungen}
Stunts können andere Stunts als Voraussetzung haben – du kannst sie erst benutzen, wenn du auch den anderen Stunt beherrschst. Daher sind sie schwieriger zu bekommen, und wenn du so einen Stunt hast, bedeutet das vermutlich, dass du den Fokus auf diesen Bereich gesetzt hast – auch wenn das heißt, dass du in anderen Bereichen dafür schwächer bist. Das balanciert den mächtigeren Stunt gegenüber anderen aus.
Es ist auch möglich, eine Voraussetzung mit einem Aspekt zu verknüpfen. Die Möglichkeit, den Aspekt zu reizen, gleicht die Überlegenheit des Stunts aus.

\subsection{Verwendung pro Spielrunde}
Außerdem könnt ihr einen Stunt begrenzen, indem ihr die Verwendung pro Spielrunde beschränkt.
\begin{commentbox}{Beispiel}
Klaus SC glaubt stark an die Geisterwelt, er möchte mit „Worte im Wind“ mit der Geisterwelt reden können um schneller an Informationen zu kommen.
Das dies nicht zu mächtig wird kann er dies nur einmal pro Spielrunde anwenden.
\end{commentbox}

\subsection{Kombinierte Grenzen}
Mancher Stunt ist trotz der obigen Begrenzungen noch zu mächtig. In so einem Fall wird mehr als eine Begrenzung notwendig, damit er im Gleichgewicht mit anderen Stunts ist.
\begin{commentbox}{Beispiel}
Frank spielt einen Soldaten, der in der Lage ist, viel Schaden auszuhalten. Neben seinem hohen Wert in Konstitution überlegt sich Frank den Stunt Spürt keinen Schmerz. Dieser erlaubt ihm, die Auswirkungen eines Angriffs zu ignorieren – einmal pro Spielsitzung und für einen Schicksal-Punkt. Frank findet diesen Stunt trotz der Begrenzungen äußerst nützlich und hebt ihn sich für Gelegenheiten auf, wenn ein Gegner mit einer schweren Waffe einen guten Treffer landet.
\end{commentbox}

\subsection{Zusammenfassung}\index{Stunt!Zusammenfassung}
\textbf{Fokusstunt:} +2 Bonus   auf   eine   Fertigkeit \linebreak
\textbf{Spezialisierung:} +4 Bonus auf eine Fertigkeit (mit Voraussetzung) \linebreak
\textbf{Fertigkeiten-tausch:} Einsatz einer Fertigkeit anstelle einer anderen \linebreak
\textbf{Verbündeter:} Du erhältst einen Gefährten den du mit Erweiterungen verbessern kannst. \linebreak
\textbf{Vorteil-Stunts:} Zusätzliche Vorteile (Ressourcen, Gegenstände, spezielle Fähigkeiten). \linebreak
\underline{Stunts können:}
\begin{itemize}
	\item an den Einsatz eines Schicksal-Punktes gebunden werden
	\item andere Stunts als Voraussetzung haben
	\item mit einem Aspekt verknüpft werden
	\item in der Häufigkeit ihres Einsatzes beschränkt werden
	\item kombinierte Grenzen haben (z. B. eine Voraussetzung und Schicksal-Punkte)
\end{itemize}

\section{Beispiele}\index{Stunt!Beispiele}
Im Folgenden findest du einige Beispiele für Stunts, die dein Charakter besitzen kann. Stunts sind üblicherweise an eine Fertigkeit gekoppelt. Du kannst auch Stunts für Fertigkeiten wählen, die du nicht einmal besitzt, allerdings solltest du dir gut überlegen, ob das sinnvoll ist.

\subsection{Ich bin immer der Erste!}
Gehört zu~\prettyref{sec:aufmerksamkeit}\linebreak
Du kannst einen Schicksal-Punkt ausgeben, um als erster in einer Runde zu handeln, ohne Rücksicht auf die Initiativ Reihenfolge zu nehmen. Wenn mehrere Beteiligte diesen Stunt haben, agiert ihr in der Reihenfolge der normalen Initiative, aber noch vor allen anderen. Wenn du in einer Runde noch nicht gehandelt hast, kannst du einen Schicksal-Punkt zahlen, um sofort und außerhalb der normalen Initiative zu handeln.
Dies kann nur zwischen den Aktionen von anderen Charakteren stattfinden und nicht als eine Unterbrechung (d. h. wenn du einen Schicksal-Punkt ausgibst und diesen Stunt benutzt, aber ein anderer Charakter noch am Zug ist, musst du warten, bis er mit seiner Aktion fertig ist). Das geht aber nur, wenn du in dieser Runde noch nicht gehandelt hast.

\subsection{Allzeit bereit}
Gehört zu~\prettyref{sec:aufmerksamkeit}\linebreak
\textit{Voraussetzung: Ich bin immer der Erste!}
Die Sinne deines Charakters sind so geschult, dass du auf Veränderungen wesentlich schneller reagierst. Für die Initiative bekommst du auf die Fertigkeit Aufmerksamkeit einen Bonus von +2. Wenn du auf einen Gegner mit der gleichen Initiative triffst, darfst du durch den Stunt zuerst handeln. Der Stunt kann mehrmals gewählt werden und erhöht jedes Mal deine Initiative um +2.

\subsection{Dickes Fell}
Gehört zu~\emph{Konstitution}\linebreak
Die meisten Charaktere mit einer guten Konstitution haben sich unter Kontrolle, wenn es kritisch wird – für dich jedoch ist Selbstkontrolle zur zweiten Natur geworden. Auch unter widrigsten Umständen oder bei gröbsten Beleidigungen zuckst du nicht einmal mit der Wimper. Außerhalb eines Kampfes kann dich nur sehr wenig erschüttern.
Du bekommst zusätzlich 2 Rüstung.

\subsection{Gefährte}\label{sec:gefährte}\index{Gefährte}
Durch diesen Stunt erhältst du einen Verbündeten, also jemanden, der dir innerhalb und außerhalb von Konflikten hilfreich zur Seite stehen kann. Gefährten bekommen ihre eigenen EP und können mit diesen gesteigert werden. Zu Beginn ist es ein Stufe 25 Begleiter, siehe dazu auch \prettyref{tab:begleiter}

\subsection{Tiergefährte}\label{sec:tiergefährte}\index{Tiergefährte}
Dein Charakter hat einen Gefährten aus dem Tierreich. Dein Tiergefährte kann aus jeder Gegend kommen. Tiergefährten bekommen ihre eigenen EP und können mit diesen gesteigert werden. Zu Beginn ist es ein Stufe 25 Begleiter, siehe dazu auch \prettyref{tab:begleiter}

\begin{commentbox}{Erläuterung}
Ein Waschbär könnte Fingerfertigkeit besitzen, da er mit seinen geschickten Händchen Dinge ganz gut manipulieren kann; ein Löwe könnte Einschüchtern besitzen. \\
Wenn dein Tiergefährte groß genug ist, kann er dir als Reittier dienen und gibt dir eine +2 auf Reiten-Proben.
\end{commentbox}